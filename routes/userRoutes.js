var express = require('express');
var router = express.Router();
const user = require('../public/users');

router.get('/users', user.getUsers);
router.get('/users/:id', user.getUserById);
router.post('/users', user.createUser);
router.put('/users/:id', user.updateUser);
router.delete('/users/:id', user.deleteUser);

module.exports = router;
