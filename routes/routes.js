var express = require('express');
var router = express.Router();

const userRouter = require('./userRoutes');
const projectRouter = require('./projectRoutes');


router.use(userRouter);
router.use(projectRouter);


module.exports = router;
