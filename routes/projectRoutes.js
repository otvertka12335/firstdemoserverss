var express = require('express');
var router = express.Router();
const project = require('../public/project');

router.get('/projects', project.getProjects);
router.get('/projects/:id', project.getProjectById);

module.exports = router;


