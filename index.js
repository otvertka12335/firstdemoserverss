var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var port = 3000;

const routes = require('./routes/routes.js');


app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(routes);


app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
});


app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});
