var client = require('./db').client;

const getProjects = (req, res) => {
    client.query(`SELECT projects.id AS id_project, projects.name, projects.description,
                    users.id, users.firstname, users.username
                    FROM public.projects
	                INNER JOIN users ON projects.user_id = users.id
	                ORDER BY id_project ASC`, (err, result) => {
        if(err) {
            res.status(400).send(err);
        }
        res.status(200).json(result.rows);
    })
};

const getProjectById = (req, res) => {
    const id = parseInt(req.params.id);

    client.query(`SELECT projects.id AS id_project, projects.name, projects.description, 
                    users.id, users.firstname, users.username 
                    FROM public.projects
	                INNER JOIN users ON projects.user_id = users.id
	                WHERE projects.id = $1`, [id], (err, result) => {
        if(err) {
            res.status(400).send(err);
        }
        res.status(200).json(result.rows);
    })
};

// const createUser = (req, res) => {
//     const { name, username } = req.body;
//
//     client.query('INSERT INTO users(name, username) VALUES($1, $2)', [name, username], (err, result) => {
//         if(err) {
//             res.status(400).send(err);
//         }
//         res.status(201).send(`User added with ID: ${result.insertId}`);
//     })
// };
//
// const updateUser = (request, response) => {
//     const id = parseInt(request.params.id);
//     const { name, username } = request.body;
//
//     client.query(
//         'UPDATE users SET name = $1, username = $2 WHERE id = $3',
//         [name, username, id],
//         (error, results) => {
//             if (error) {
//                 throw error
//             }
//             response.status(200).send(`User modified with ID: ${id}`)
//         }
//     )
// };
//
// const deleteUser = (request, response) => {
//     const id = parseInt(request.params.id);
//
//     client.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
//         if (error) {
//             throw error
//         }
//         response.status(200).send(`User deleted with ID: ${id}`)
//     })
// };

module.exports = {
    getProjects,
    getProjectById,
};
