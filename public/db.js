const { Client } = require('pg');
const connectionString = 'postgres://postgres:postgres@localhost:5432/postgres';
const client = new Client({
    connectionString: connectionString
});
client.connect();
// var conString = 'postgres://postgres:postgresql@localhost:57036/firstdemo';
module.exports = {
  client
};
